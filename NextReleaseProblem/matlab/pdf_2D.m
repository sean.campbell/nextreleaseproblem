%%
%clear all;
%close all;
clc;

e = '\\studenthome.cis.strath.ac.uk\homes\system\Windows\My Documents\ForMatlab.txt';
fPath = 'C:\Users\Sean\Documents\ForMatlab.csv';
%% File reading and sorting
%strct = tdfread(fpath);
% mat = [strct.Cost, strct.Score];
mat = csvread(fPath);
mat = sortrows(mat, 1);
%clear strct;

%% Scatter 3D
scatter3(mat(:, 1), mat(:, 2), mat(:, 3));

%% Brief plot of 2D front; don't bother writing NSGA-II
% figure;
% plot(mat(:, 1), mat(:, 2), '.b'), hold on;
%  
% p = polyfit(mat(:, 1), mat(:, 2), 1);
% r = p(1).*mat(1) + p(2);
%  
% plot(mat(:, 1), r, '--g');
% hold off;
 
%% PDF Caluclation for two variables
% mat(:, 2) = mat(:, 2).*100;
% mat(:, 1) = mat(:, 1);%./100;
% mu = mean(mat);
% sigma = std(mat);
%  
% [X, Y] = meshgrid(mat(:, 1), mat(:, 2));
% F = mvnpdf([X(:) Y(:)], mu, sigma);
% F = reshape(F, length(mat),length(mat));
% figure;
% surf(mat(:, 1), mat(:, 2), F, 'EdgeColor', 'none'), hold on;
% shading interp
% lighting gouraud
% alpha(0.9)
% xlim([min(mat(:, 1)) + 170, max(mat(:, 1)) - 170])
% %ylim([0.0 100.0]);
% hold off;
% xlabel('Cost');
% ylabel('Score %');
% zlabel('Probability');
% title('Simple Cost/Score Evaluator with dependencies');
%  
%  

