package problem;

import java.util.Random;

import org.opt4j.core.common.random.RandomMersenneTwister;
import org.opt4j.core.genotype.BooleanGenotype;
import org.opt4j.core.problem.Creator;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class RequirementSetCreator implements Creator<BooleanGenotype> {
	
	private final Random rng;
	private final int genotypeLength;
	
	@Inject
	public RequirementSetCreator(@Named(value = "Problem") IRequirementsProblem problem) {
		this.rng = new RandomMersenneTwister(problem.getSeed());	
		this.genotypeLength = problem.getParser().getRequirementCount();
	}

	@Override
	public BooleanGenotype create() {
		final BooleanGenotype ret = new BooleanGenotype();
		ret.init(this.rng, this.genotypeLength);
		return ret;
	}
}
