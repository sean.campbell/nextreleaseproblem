package problem;

import java.util.List;

import org.opt4j.core.Objectives;
import org.opt4j.core.Objective.Sign;
import org.opt4j.core.problem.Evaluator;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import parser.IRequirement;

public class SimpleRequirementSetEvaluator implements Evaluator<List<IRequirement>> {
	
	@Inject
	public SimpleRequirementSetEvaluator(@Named(value = "Problem") IRequirementsProblem problem) {
		// Nothing required from the problem for this evaluator
		System.out.println("Budget of:\t" + problem.getBudget());
	}
	
	@Override
	public Objectives evaluate(final List<IRequirement> phenotype) {
		final int cost = phenotype.stream().mapToInt(req -> req.getCost()).sum();
		final double score = phenotype.stream().mapToDouble(req -> req.getTotalWeight(true).doubleValue()).sum();
		
		final Objectives ret = new Objectives();
		ret.add("Cost", Sign.MIN, cost);
		ret.add("Score", Sign.MAX, score);
		return ret;
	}
}
