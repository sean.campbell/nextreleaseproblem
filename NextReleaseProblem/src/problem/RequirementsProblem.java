package problem;

import java.io.IOException;

import javax.inject.Named;

import org.opt4j.core.start.Constant;

import com.google.inject.Inject;

import parser.IStatefulParser;

public class RequirementsProblem implements IRequirementsProblem {
	
	private final IStatefulParser parser;
	private final long seed;
	private final double budgetMultiplier;
	
	@Inject
	public RequirementsProblem(@Named(value = "Parser") IStatefulParser parser,
			@Constant(value="Seed") long seed,
			@Constant(value="Budget Multiplier") double budgetMultiplier) throws IOException{
		this.parser = parser;
		this.seed = seed;
		this.budgetMultiplier = budgetMultiplier;
	}
	
	@Override
	public IStatefulParser getParser(){
		return this.parser;
	}
	
	@Override
	public long getSeed(){
		return this.seed;
	}
	
	@Override
	public double getBudget(){
		return this.budgetMultiplier*this.parser.getTotalCost();
	}

}
