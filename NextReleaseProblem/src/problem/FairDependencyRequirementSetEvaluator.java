package problem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.opt4j.core.Objectives;
import org.opt4j.core.Objective.Sign;
import org.opt4j.core.problem.Evaluator;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import parser.IRequirement;

public class FairDependencyRequirementSetEvaluator implements Evaluator<List<IRequirement>> {

	private final Map<Integer, Set<IRequirement>> custRequirementLut;
	
	@Inject
	public FairDependencyRequirementSetEvaluator(@Named(value = "Problem") IRequirementsProblem problem) {
		System.out.println("Budget of:\t" + problem.getBudget());
		this.custRequirementLut = problem.getParser().getCustomerRequirementMapping();
	}
	
	@Override
	public Objectives evaluate(List<IRequirement> phenotype) {
		final Set<IRequirement> newPheno = getAllDependentRequirements(phenotype);
		final int cost = newPheno.stream().mapToInt(req -> req.getCost()).sum();
		final double weight = newPheno.stream().mapToDouble(req -> req.getTotalWeight(true).doubleValue()).sum();
		final double custSatDev = customerSatisfactionStandardDeviation(newPheno);
		
		final Objectives ret = new Objectives();
		ret.add("Cost", Sign.MIN, cost);
		ret.add("Score", Sign.MAX, weight);
		ret.add("Fairness (Customer Satisfaction Deviation)", Sign.MIN, custSatDev);
		return ret;
	}

	private double customerSatisfactionStandardDeviation(Set<IRequirement> newPheno) {
		final Set<Integer> reqIds = newPheno.stream().mapToInt(i -> i.getID()).boxed()
				.collect(Collectors.toSet());
		
		final List<Double> happiness = new ArrayList<>(this.custRequirementLut.size());
		for(Map.Entry<Integer, Set<IRequirement>> e : this.custRequirementLut.entrySet()){
			final int total = e.getValue().size();
			final Set<Integer> cp = e.getValue().stream().mapToInt(i -> i.getID()).boxed()
														.collect(Collectors.toSet());
			cp.retainAll(reqIds);
			final int satisfied = cp.size();
			happiness.add((double)satisfied/total);
		}
		final double happinessMean = happiness.stream().mapToDouble(Double::doubleValue).sum()/happiness.size();
		final double std = Math.sqrt(happiness.stream()
										.mapToDouble(d -> Math.pow(happinessMean - d, 2))
											.sum()/happiness.size());
		return std;
	}
	
	private Set<IRequirement> getAllDependentRequirements(final List<IRequirement> phenotype){
		final Set<IRequirement> ret = new HashSet<>(phenotype);
		for(IRequirement r : phenotype){
			ret.addAll(r.getDependencies(true));
		}
		return ret;
	}
}
