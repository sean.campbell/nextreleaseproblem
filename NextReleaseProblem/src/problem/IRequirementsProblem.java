package problem;

import parser.IStatefulParser;

public interface IRequirementsProblem {

	IStatefulParser getParser();
	long getSeed();
	double getBudget();
}