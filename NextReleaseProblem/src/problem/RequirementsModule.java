package problem;

import org.opt4j.core.problem.ProblemModule;
import org.opt4j.core.start.Constant;

import com.google.inject.name.Names;

import parser.IStatefulParser;
import parser.StatefulParser;

public class RequirementsModule extends ProblemModule {
	
	@Constant(value = "File Path")
	private String filePath = "";
	
	@Constant(value = "Seed")
	private long seed = 10L;
		
	@Constant(value = "Budget Multiplier")
	private double budgetMultiplier = 0.5d;
	
	
	public RequirementsModule(final String filePath, final long seed, final double budgetMultiplier){
		this.filePath = filePath;
		this.seed = seed;
		this.budgetMultiplier = budgetMultiplier;
	}
	
	public RequirementsModule(){
	}
	
	public String getFilePath(){
		return this.filePath;
	}
	
	public void setFilePath(final String filePath){
		this.filePath = filePath;
	}
	
	public long getSeed(){
		return this.seed;
	}
	
	public void setSeed(final long seed){
		this.seed = seed;
	}
	
	public double getBudgetMultiplier(){
		return this.budgetMultiplier;
	}
	
	public void setBudgetMultiplier(final double budgetMultiplier){
		this.budgetMultiplier = budgetMultiplier;
	}
	
	@Override
	protected void config() {
		bind(IStatefulParser.class).annotatedWith(Names.named("Parser")).to(StatefulParser.class);
		bind(IRequirementsProblem.class).annotatedWith(Names.named("Problem")).to(RequirementsProblem.class);
		bindProblem(RequirementSetCreator.class,
				RequirementSetDecoder.class,
				//SimpleRequirementSetEvaluator.class
				//DependencyRequirementSetEvaluator.class
				FairDependencyRequirementSetEvaluator.class
				//FairResourceDependencyRequirementSetEvaluator.class
				//SingleObjectiveRequirementSetEvaluator.class
				//SimpleRequirementSetEvaluator.class
				);
	}	
}
