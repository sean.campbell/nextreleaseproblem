package problem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.opt4j.core.Objectives;
import org.opt4j.core.Objective.Sign;
import org.opt4j.core.problem.Evaluator;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import parser.IRequirement;

// See http://www0.cs.ucl.ac.uk/staff/mharman/PastMScProjects2006/JianRen.pdf for an explanation
// of the fairness measure
public class FairResourceDependencyRequirementSetEvaluator implements Evaluator<List<IRequirement>> {

	private final Map<Integer, Set<IRequirement>> custRequirementLut;
	private final Map<Integer, Integer> custCostLut;
	
	@Inject
	public FairResourceDependencyRequirementSetEvaluator(@Named(value = "Problem") IRequirementsProblem problem) {
		System.out.println("Budget of:\t" + problem.getBudget());
		int sz = problem.getParser().getCustomerRequirementMapping().size();
		this.custRequirementLut = new HashMap<>(sz);
		this.custCostLut = new HashMap<>(sz);
		
		initMaps(problem.getParser().getCustomerRequirementMapping());
	}

	private void initMaps(final Map<Integer, Set<IRequirement>> customerRequirementMapping) {
		for(Map.Entry<Integer, Set<IRequirement>> e : customerRequirementMapping.entrySet()){
			final Set<IRequirement> allActualReqs = getAllDependentRequirements(e.getValue());
			this.custRequirementLut.put(e.getKey(), allActualReqs);
			this.custCostLut.put(e.getKey(), allActualReqs.stream().mapToInt(i -> i.getCost()).sum());
		}
	}

	@Override
	public Objectives evaluate(List<IRequirement> phenotype) {
		final Set<IRequirement> newPheno = getAllDependentRequirements(phenotype);
		final int cost = newPheno.stream().mapToInt(req -> req.getCost()).sum();
		final double weight = newPheno.stream().mapToDouble(req -> req.getTotalWeight(true).doubleValue()).sum();
		final double fairness = customerSatisfaction(newPheno);
		
		final Objectives ret = new Objectives();
		ret.add("Cost", Sign.MIN, cost);
		ret.add("Score", Sign.MAX, weight);
		ret.add("Fairness (Fair Resource Distribution)", Sign.MIN, fairness);
		return ret;
	}

	private double customerSatisfaction(final Set<IRequirement> reqs) {
		final List<Integer> actualCosts = new ArrayList<>(this.custRequirementLut.size());
		for(Map.Entry<Integer, Set<IRequirement>> e : this.custRequirementLut.entrySet()){
			final Set<IRequirement> cp = new HashSet<>(e.getValue());
			cp.retainAll(reqs);
			final int actualCost = cp.stream().mapToInt(i -> i.getCost()).sum();
			actualCosts.add(actualCost);
		}
		// Normalise the values; lazy by sorting
		Collections.sort(actualCosts);
		final int min = actualCosts.get(0), denom = actualCosts.get(actualCosts.size() - 1) - min;
		final List<Double> normCosts = 
				actualCosts.stream().mapToDouble(i -> ((i.doubleValue() - min)/denom)).boxed()
									.collect(Collectors.toList());
		
		final double fairnessMean = normCosts.stream().mapToDouble(i -> i.doubleValue()).sum()/normCosts.size();
		final double std = Math.sqrt(normCosts.stream()
										.mapToDouble(d -> Math.pow(fairnessMean - d, 2))
											.sum()/normCosts.size());
		return std;
	}
	
	private Set<IRequirement> getAllDependentRequirements(final Collection<IRequirement> param){
		final Set<IRequirement> ret = new HashSet<>(param);
		for(IRequirement r : param){
			ret.addAll(r.getDependencies(true));
		}
		return ret;
	}

}
