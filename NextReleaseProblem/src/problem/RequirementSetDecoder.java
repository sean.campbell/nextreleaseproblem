package problem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.opt4j.core.genotype.BooleanGenotype;
import org.opt4j.core.problem.Decoder;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import parser.IRequirement;

public class RequirementSetDecoder implements 
					Decoder<BooleanGenotype, List<IRequirement>> {
	
	private final Map<Integer, IRequirement> lut;
	
	@Inject
	public RequirementSetDecoder(@Named(value = "Problem") IRequirementsProblem problem){
		this.lut = problem.getParser().getRequirements();
	}

	@Override
	public List<IRequirement> decode(BooleanGenotype genotype) {		
		return IntStream.range(0, genotype.size())
			.filter((i) -> genotype.get(i))
			.mapToObj((i) -> this.lut.get(i + 1))  // Assumption: ID = idx + 1
			.collect(Collectors.toCollection(ArrayList::new));
	}
}
