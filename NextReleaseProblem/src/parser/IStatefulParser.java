package parser;

import java.util.Map;
import java.util.Set;

public interface IStatefulParser {
	// Required by Decoder
	Map<Integer, IRequirement> getRequirements();
	
	// Required by Creator to know genotype length
	int getRequirementCount();
	
	// Required by Evaluator to calculate the budget
	int getTotalCost();
	
	// Useful for some Evaluators
	Map<Integer, Set<IRequirement>> getCustomerRequirementMapping();
}
