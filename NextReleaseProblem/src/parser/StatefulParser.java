package parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.opt4j.core.start.Constant;

import com.google.inject.Inject;

public class StatefulParser implements IStatefulParser {
	// State info
	private Map<Integer, IRequirement> requirements;
	private Map<Integer, Set<IRequirement>> custReqMapping;
	private int reqCount, totalCost;
	
	@Inject
	public StatefulParser(@Constant(value = "File Path")String filePath) throws IOException{
		initialiseState();
		try(BufferedReader br = new BufferedReader(new FileReader(filePath))){
			parseRequirements(br);
			addDependencies(br);
			parseCustomers(br);
			
		}
	}
	
	private void initialiseState() {
		this.requirements = new HashMap<>();
		this.custReqMapping = new HashMap<>();
		this.reqCount = this.totalCost = 0;
	}
	
	private void parseRequirements(final BufferedReader br)
				throws NumberFormatException, IOException{
		final int levelCount = Integer.parseUnsignedInt(br.readLine().trim()) + 1;
		
		String ref1, ref2;
		int id = 1;
		for(int level = 1; level != levelCount; level++){
			
			if(null != (ref1 = br.readLine()) && null != (ref2 = br.readLine())){
				final int reqs = Integer.parseUnsignedInt(ref1);
				final String[] args = ref2.split("\\s+");
				assert reqs == args.length; // Error checking
				
				for(int j = 0; j != reqs; j++){
					IRequirement req = factoryMethod(id++, level, Integer.parseUnsignedInt(args[j]));
					this.requirements.put(req.getID(), req);
					this.totalCost += req.getCost();
				}
				this.reqCount += reqs;
			}
		}
		
		// Build transitive relationships
		for(Map.Entry<Integer, IRequirement> e : this.requirements.entrySet()){
			e.getValue().buildTransitiveDependencies();
		}
	}
	
	private void addDependencies(final BufferedReader br) throws NumberFormatException, IOException{
		final int dependCount = Integer.parseUnsignedInt(br.readLine().trim());
		
		for(int i = 0; i != dependCount; i++){
			int[] elems = parseIntArray(br.readLine().split("\\s+"));
			assert elems.length == 2;
			this.requirements.get(elems[0]).addDependency(this.requirements.get(elems[1]));
		}
	}
	
	private void parseCustomers(final BufferedReader br) throws NumberFormatException, IOException{
		final int custCount = Integer.parseUnsignedInt(br.readLine().trim());
		final List<Integer> weights = new ArrayList<>(custCount);
		
		for(int i = 0; i != custCount; i++){
			int[] elems = parseIntArray(br.readLine().split("\\s+"));
			assert elems[1] == elems.length - 2;
			for(int j = 2, len = elems.length; j != len; j++){
				this.requirements.get(elems[j]).addCustomerWeight(i, elems[0]);
				weights.add(elems[0]);
			}
			this.custReqMapping.put(i + 1, IntStream.range(2, elems.length)
					.mapToObj(idx -> this.requirements.get(elems[idx])).collect(Collectors.toSet()));	
		}
		
		int totalWeight = weights.stream().mapToInt(Integer::intValue).sum();
		addTotalWeight(this.requirements, totalWeight);
	}
	
	private static void addTotalWeight(final Map<Integer, IRequirement> ret, final int sum) {
		for(IRequirement r : ret.values()){
			r.setTotalCustomerWeight(sum);
		}
	}
	
	private static int[] parseIntArray(final String[] arr) {
	    return Stream.of(arr).mapToInt(Integer::parseInt).toArray();
	}
	
	private IRequirement factoryMethod(final int id, final int level, final int cost){
		return new Requirement(id, level, cost);
	}
	
	@Override
	public Map<Integer, IRequirement> getRequirements() {
		return Collections.unmodifiableMap(this.requirements);
	}

	@Override
	public int getRequirementCount() {
		return this.reqCount;
	}

	@Override
	public int getTotalCost() {
		return this.totalCost;
	}

	@Override
	public Map<Integer, Set<IRequirement>> getCustomerRequirementMapping() {
		return Collections.unmodifiableMap(this.custReqMapping);
	}
}
