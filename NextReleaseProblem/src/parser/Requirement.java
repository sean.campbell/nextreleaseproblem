package parser;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Requirement implements IRequirement {
	private final int id, level, cost;
	
	private final Set<IRequirement> directDependencies, allDependencies;
	
	private final Map<Integer, Number> customerWeights;
	private final Map<Integer, Number> normCustWeights;
	
	private int totalWeight, totalCustWeight;
	
	public Requirement(final int id, final int level, final int cost){
		this.id = id;
		this.level = level;
		this.cost = cost;
		
		this.directDependencies = new HashSet<>();
		// Due to lazy init, be careful if in parallel mode
		this.allDependencies = Collections.newSetFromMap(new ConcurrentHashMap<IRequirement, Boolean>());
		
		this.customerWeights = new HashMap<>();
		this.normCustWeights = new HashMap<>();
		
		this.totalWeight = this.totalCustWeight = 0;
	}
	
	@Override
	public int getID() {
		return this.id;
	}

	@Override
	public int getLevel() {
		return this.level;
	}

	@Override
	public int getCost() {
		return this.cost;
	}

	@Override
	public void addDependency(final IRequirement dependency) {
		this.directDependencies.add(dependency);
	}
	
	@Override
	public Set<IRequirement> getDependencies(boolean transitive) {
		Set<IRequirement> ret = null;
		if(transitive){
			// Try to call buildTransitiveDependencies();
			// may fail with a concurrent modification exception if unlucky with thread scheduling
			// so recommend it is performed before-hand
			if(this.allDependencies.size() == 0){
				this.addRequirementsRecursively(this.directDependencies);
			}
			ret = Collections.unmodifiableSet(this.allDependencies);
		}
		else{
			ret = Collections.unmodifiableSet(this.directDependencies);
		}
		return ret;
	}
	
	@Override
	public void buildTransitiveDependencies() {
		this.addRequirementsRecursively(this.directDependencies);
	}
	
	private void addRequirementsRecursively(final Set<IRequirement> reqs){
		for(IRequirement r : reqs){
			if(!this.allDependencies.contains(r)){
				this.allDependencies.add(r);
				addRequirementsRecursively(reqs);
			}
		}
	}
	
	@Override
	public Map<Integer, Number> getCustomerWeights(boolean norm) {
		Map<Integer, Number> ref = null;
		if(!norm)
			ref = Collections.unmodifiableMap(this.customerWeights);
		else{
			// Lazy init of normCustMap provided totalCustWeight given
			if(this.normCustWeights.size() == 0  && this.totalCustWeight != 0){
				for(Map.Entry<Integer, Number> e : this.customerWeights.entrySet()){
					this.normCustWeights.put(e.getKey(), e.getValue().doubleValue()/this.totalCustWeight);
				}
			}
			ref = Collections.unmodifiableMap(this.normCustWeights);
		}
		return ref;
	}

	@Override
	public Number getTotalWeight(final boolean norm) {
		return (norm) ? (double)this.totalWeight/this.totalCustWeight : this.totalWeight;
	}
		
	@Override
	public void addCustomerWeight(int customer, int weight) {
		if(this.customerWeights.containsKey(customer)){
			throw new UnsupportedOperationException("Customer " + customer + " with weight added twice!");
		}
		this.customerWeights.put(customer, weight);
		this.totalWeight += weight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getID();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IRequirement other = (IRequirement) obj;
		if (this.getID() != other.getID())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("%d", id);
		//return String.format("Requirement [id=%s, level=%s, cost=%s, dependencies=%s, weight=%s]",
		//		id, level, cost, directDependencies.toString(), totalWeight);
	}

	@Override
	public void setTotalCustomerWeight(int totalCustWeight) {
		this.totalCustWeight = totalCustWeight;
	}

	@Override
	public int getCostWithDependencies() {
		if(this.allDependencies.size() == 0){
			this.addRequirementsRecursively(this.directDependencies);
		}
		int cost = this.cost;
		for(IRequirement r : this.allDependencies){
			cost += r.getCost();
		}
		return cost;
	}
}
