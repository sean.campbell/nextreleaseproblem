package parser;

import java.util.Map;
import java.util.Set;

public interface IReq {
	// General Info
	int getID();
	int getLevel();
	
	// Cost information
	int getCost();
	
	// Dependency information
	void addDependency(IRequirement req);
	Set<IRequirement> getDependencies(boolean transitive);
	
	// Score information
	void addCustomerWeight(int customer, int weight);
	void setTotalCustomerWeight(int totalCustWeight);
	Map<Integer, Number> getCustomerWeights(boolean norm);
	Number getTotalWeight(boolean norm);
}
