package parser;

import java.util.Map;
import java.util.Set;

public interface IRequirement {
	// General Info
	int getID();
	int getLevel();
	
	// Cost information
	int getCost();
	int getCostWithDependencies();
	
	// Dependency information
	void addDependency(IRequirement req);
	void buildTransitiveDependencies();
	Set<IRequirement> getDependencies(boolean transitive);
	
	// Score information
	void addCustomerWeight(int customer, int weight);
	void setTotalCustomerWeight(int totalCustWeight);
	Map<Integer, Number> getCustomerWeights(boolean norm);
	Number getTotalWeight(boolean norm);
}
