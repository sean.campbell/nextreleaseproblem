package main;

import org.opt4j.core.Individual;
import org.opt4j.core.optimizer.Archive;
import org.opt4j.core.problem.ProblemModule;
import org.opt4j.core.start.Opt4JTask;
import org.opt4j.optimizers.ea.EvolutionaryAlgorithmModule;
import org.opt4j.viewer.ViewerModule;

import problem.RequirementsModule;

// Fast problem runner; use the application for superior results through parameter tuning
public class GA_Runner {
	
	private static final String FILE_NAME = "nrp1.txt";
	
	// Problem Configuration
	private static final String FILE_PATH = System.getProperty("user.dir") + "\\" + FILE_NAME;
	private static final long SEED = 500L;
	private static final double BUDGET_MULTIPLIER = 0.7d;
	
	// EA Configuration
	private static final int POPULATION_SIZE = 500;
	private static final int GENERATION_COUNT = 1000;
	private static final double CROSSOVER_PROB = 0.85d;
		
	public static void main(String[] args){
		final Opt4JTask task = new Opt4JTask(false);
		task.init(configureEA(), configureProblem(), configureViewer());
		
		try {
	        task.execute();
	        Archive archive = task.getInstance(Archive.class);
	        for (Individual individual : archive) {
	                System.out.println(individual.toString());
	        }
		} catch (Exception e) {
	        e.printStackTrace();
		} finally {
	        task.close();
		} 
		
	}
	
	private static EvolutionaryAlgorithmModule configureEA(){
		final EvolutionaryAlgorithmModule ret = new EvolutionaryAlgorithmModule();
		ret.setAlpha(POPULATION_SIZE);
		ret.setGenerations(GENERATION_COUNT);
		ret.setCrossoverRate(CROSSOVER_PROB);
		return ret;
	}
	
	private static ProblemModule configureProblem(){
		return new RequirementsModule(GA_Runner.FILE_PATH, GA_Runner.SEED, GA_Runner.BUDGET_MULTIPLIER);
	}
	
	private static ViewerModule configureViewer(){
		ViewerModule ret = new ViewerModule();
		ret.setCloseOnStop(false);
		ret.setTitle("EA Viewer");
		return ret;
	}
}
